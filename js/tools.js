
var applyIf = function(args, defaults){
    for (var key in defaults) {
        if (!args.hasOwnProperty(key) || args[key] == null) {
            args[key] = defaults[key];
        }
    }

}

var controllInputData = function (data, conditions) {
    data = data*1
    if (typeof data === "number") {
        if (conditions != null) {
            if (data >= conditions.min && data <= conditions.max) {
                return true
            } else {
                return false
            }
        }
    }
}

var getBlock = function (id) {
    return null != id ? document.getElementById(id) : null;
}


var setText = function(block, txt) {
    if (null != txt && txt.length > 0) {
        block.appendChild(document.createTextNode(txt));
    }
    else {
        if (null != this.firstChild) {
            block.firstChild.remove();
        }
    }
};

var clearInputFields = function (block) {
    block.value = "";
}

var conditionRules = {
    carValue: {min: 100, max: 100000},
    taxPercentage: {min: 0, max: 100},
    instalments: {min: 1, max: 13}
}

var createBlock = function(args) {
    applyIf(args, {
        cls: '',
        val: '',
        elem: 'div'
    });

    var block = document.createElement(args.elem);

    if (args.id != null) {
        block.id = args.id;
    }

    if (args.cls != null) {
        block.className = args.cls;
    }

    block.setText = function(txt) {
        if (null != txt && txt.length > 0) {
            this.appendChild(document.createTextNode(txt));
        }
        else {
            if (null != this.firstChild) {
                this.firstChild.remove();
            }
        }
    };


    block.removeBlock = function() {
        this.remove();
    };

    block.appendBlock = function(block) {
        if (null != block) {
            this.appendChild(block);
        }
    };

    block.setClass = function(arg) {
        this.className = arg;
    };

    return block;
}
