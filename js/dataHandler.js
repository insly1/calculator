
var sendDataForCalculating = function () {
    var carCost = getBlock('carCost').value;
    var taxPercentage = getBlock('taxPercentage').value;
    var numberOfPayments = getBlock('numberOfPayments').value;

    clearInputFields(getBlock('carCost'));
    clearInputFields(getBlock('taxPercentage'));
    clearInputFields(getBlock('numberOfPayments'));

    if (controllInputData(carCost, conditionRules.carValue) === true
        && controllInputData(taxPercentage, conditionRules.taxPercentage) === true
        && controllInputData(numberOfPayments, conditionRules.instalments) === true) {
        var args = {carCost: carCost, taxPercentage: taxPercentage, numberOfPayments: numberOfPayments}
        getBlock('output').innerHTML="";
        doRequest({url: "src/serverResponse.php", method: 'post', params: args}, function (responseData) {
            getBlock('output').appendChild(output(responseData, args));
            args = null;
        })
    } else {
        getBlock('output').innerHTML="";
        setText(getBlock('output'), "Something went wrong ! ");
    }
}