
var doRequest = function(args, cbk) {
    cbk = cbk || function(data){};
    applyIf(args, {
        method: "GET",
        url: "",
        params: ""

    });

    var myJson = JSON.stringify(args.params);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText) {
                var myObj = JSON.parse(this.responseText);
                cbk(myObj);
            }
        }
        else if (null != args.cbk)
        {
            args.cbk({});
        }
    };

    xhttp.open(args.method, args.url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send('data='+myJson);

}
