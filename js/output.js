
var output = function (data, args) {
    var table = createBlock({cls: "table", elem: 'table'});
    var thead = createBlock({cls: "thead-light", elem: 'thead'});
    var tr = createBlock({elem: 'tr'});
    var value = createBlock({elem: 'th'});
    var base = createBlock({elem: 'th'});
    var commission = createBlock({elem: 'th'});
    var tax = createBlock({elem: 'th'});
    var total = createBlock({elem: 'th'});

    value.setText("Value");
    base.setText("Base premium " + "(" + data.main.policyPrice * 100 + "%)");
    commission.setText("Commision" + "(" + data.main.commisionPercentage * 100 + "%)");
    tax.setText("Tax" + "(" + args.taxPercentage + "%)");
    total.setText("Total");

    tr.appendBlock(value);
    tr.appendBlock(base);
    tr.appendBlock(commission);
    tr.appendBlock(tax);
    tr.appendBlock(total);

    thead.appendBlock(tr);

    table.appendBlock(thead);

    var tbody = createBlock({elem: 'tbody'});
    var tBodyTR = createBlock({elem: 'tr'});
    var valueValue = createBlock({elem: 'th'});
    var baseValue = createBlock({elem: 'th'});
    var commissionValue = createBlock({elem: 'th'});
    var taxValue = createBlock({elem: 'th'});
    var totalValue = createBlock({elem: 'th'});

    valueValue.setText(String(args.carCost));
    baseValue.setText(String(data.main.base));
    commissionValue.setText(String(data.main.commision));
    taxValue.setText(String(data.main.tax));
    var totalValueSumm = (data.main.base + data.main.commision + data.main.tax + (args.carCost * 1)).toFixed(2);
    totalValue.setText(String(totalValueSumm));

    tBodyTR.appendBlock(valueValue);
    tBodyTR.appendBlock(baseValue);
    tBodyTR.appendBlock(commissionValue);
    tBodyTR.appendBlock(taxValue);
    tBodyTR.appendBlock(totalValue);

    tbody.appendBlock(tBodyTR);

    var countData = 0;
    for (let key in data) {
        countData++;
    }

    if (countData > 1) {
        for (let key in data) {
            if (key != "main") {
                var tBodyTRLoop = createBlock({elem: 'tr'});
                var valueValueLoop = createBlock({elem: 'td'});
                var baseValueLoop = createBlock({elem: 'td'});
                var commissionValueLoop = createBlock({elem: 'td'});
                var taxValueLoop = createBlock({elem: 'td'});
                var totalValueLoop = createBlock({elem: 'td'});

                valueValueLoop.setText(String("Instalment " + (key * 1 + 1)));
                baseValueLoop.setText(String(data[key].base.toFixed(2)));
                commissionValueLoop.setText(String(data[key].commision.toFixed(2)));
                taxValueLoop.setText(String(data[key].tax.toFixed(2)));
                var totalValueSummLoop = (data[key].base + data[key].commision + data[key].tax).toFixed(2);
                totalValueLoop.setText(String(totalValueSummLoop));

                tBodyTRLoop.appendBlock(valueValueLoop);
                tBodyTRLoop.appendBlock(baseValueLoop);
                tBodyTRLoop.appendBlock(commissionValueLoop);
                tBodyTRLoop.appendBlock(taxValueLoop);
                tBodyTRLoop.appendBlock(totalValueLoop);

                tbody.appendBlock(tBodyTRLoop);
            }
        }
    }

    table.appendBlock(tbody);

    return table;
}