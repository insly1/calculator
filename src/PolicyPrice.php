<?php

class PolicyPrice {

    private $policyPrice;
    private $policyAvarage = 0.11; // Base price of policy is 11% from entered car value
    private $policyUserTime = 0.13; // every Friday 15-20 o’clock (user time) when it is 13%

    public function setPolicyPrice($price) {
        $this->policyPrice = $price;
    }

    public function getPolicyPrice() {
        if ($this->getDayOfTheWeekAndTime() == true) {
            $this->setPolicyPrice($this->policyUserTime);
        } else {
            $this->setPolicyPrice($this->policyAvarage);
        }
        return $this->policyPrice;
    }

    public function getDayOfTheWeekAndTime() {
        // gets day of week as number(0=sunday,1=monday...,6=sat)
        $day = date('w');
        $hours = date('H');

        if ($day == 5 and ($hours > 15 or $hours < 20)) {
            return true;
        }
    }

}