<?php

class ControllOutputData {
    public function makeControll($array) {
        // Variables for controll loop
        $index = 0;
        $base = 0;
        $commision = 0;
        $tax = 0;

        // Control loop
        while ($index < (count($array) - 1)) {
            $base += $array[$index]['base'];
            $commision += $array[$index]['commision'];
            $tax += $array[$index]['tax'];
            $index++;
        }

        // Fixing output data
        $fixedBase = $array[0]['base'] + ($array['main']['base'] - $base);
        $fixedCommision = $array[0]['commision'] + ($array['main']['commision'] - $commision);
        $fixedTax = $array[0]['tax'] + ($array['main']['tax'] - $tax);
        $array[count($array) - 2]['base'] = $fixedBase;
        $array[count($array) - 2]['commision'] = $fixedCommision;
        $array[count($array) - 2]['tax'] = $fixedTax;

        return $array;
    }
}