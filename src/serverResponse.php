<?php
require ("../src/PolicyPrice.php");
require ("../src/Calculator.php");
require ("../src/ControllOutputData.php");

if (isset($_REQUEST['data'])) {
    $userInput = json_decode($_REQUEST['data'], true);

    // String -> int
    $userInput['carCost'] = $userInput['carCost'] * 1;
    $userInput['taxPercentage'] = $userInput['taxPercentage'] * 1;
    $userInput['numberOfPayments'] = $userInput['numberOfPayments'] * 1;

    // Logig
    $calculator = new Calculator();
    $response = json_encode($calculator -> calculate($userInput));

    // Response
    echo $response;
}