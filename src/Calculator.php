<?php

class Calculator {

    public function calculate($userInput) {
        $commisionPercentage = 0.17;

        // Getting Policy percentage
        $policyPrice = new PolicyPrice();
        $basePrice = round($userInput['carCost'] * $policyPrice->getPolicyPrice(), 2);

        // Calculating output
        $commision = round($basePrice * $commisionPercentage, 2);
        $tax = round($basePrice * ($userInput['taxPercentage'] / 100), 2);
        $outputArr = ['main' => ['base' => $basePrice, 'commision' => $commision, 'tax' => $tax, 'policyPrice' => $policyPrice->getPolicyPrice(),
            'commisionPercentage' => $commisionPercentage]];

        // Calculate output data if there is more than 1 instalment
        if ( $userInput['numberOfPayments'] > 1 ) {
            for ($i = 0; $i < $userInput['numberOfPayments']; $i++) {
                $arr = ['base' => round($basePrice / $userInput['numberOfPayments'], 2),
                    'commision' => round($commision / $userInput['numberOfPayments'],2),
                    'tax' => round($tax / $userInput['numberOfPayments'], 2)];
                array_push($outputArr, $arr);
            }

            // Make controll if output data is right if not make changes
            $controllOutputData = new ControllOutputData();
            $outputArr = $controllOutputData -> makeControll($outputArr);
        }

        return $outputArr;
    }
}